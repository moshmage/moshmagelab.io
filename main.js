/**
 * Created by Mosh Mage on 6/2/2016.
 */
angular.module('mmConsole',['ngRoute','SocketIO'])
    .config(function($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html'
            });
    });