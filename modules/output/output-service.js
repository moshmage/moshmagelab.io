/**
 * Created by Mosh Mage on 6/2/2016.
 */
angular.module('mmConsole').factory('OutputService',[
    'ViewsConstant',
    function (viewsConstant) {
        var newView = {view:''};
        var viewLines = [];
        var CONST_MAXVIEWS = 15;
        
        function resetView() { newView = {view:''}; }
        function resetLines() {
            viewLines = [];
        }

        function pushLine(line) {
            viewLines.push(line);
            if (viewLines.length > CONST_MAXVIEWS) {
                viewLines.splice(0,1);
            }

            resetView();
        }

        function showView(viewName, ret) {
            var cmd = viewName;
            if (!viewsConstant[viewName]) {
                viewName = 'unknown';
            }

            if (viewsConstant[viewName].view) {
                pushLine({view: viewsConstant[viewName].view, cmd: cmd});
            }

            if (ret) {
                return newView.view;
            }
        }

        return {
            getLines: function () { return viewLines },
            getLastView: function () { return newView },
            show: showView,
            pushLine: pushLine,
            resetView: resetView,
            resetLines: resetLines
        }
    }
]);
