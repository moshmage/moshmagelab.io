/**
 * Created by Mosh Mage on 6/2/2016.
 */
angular.module('mmConsole').controller('OutputController',[
    '$scope',
    'OutputService', '$window',
    function ($scope,outputService) {
        var context = this;
        context.check = {};
        context.events = {};
        context.data = {
            newView: outputService.getLastView(),
            lines: outputService.getLines()
        };

        function scrollToInput() {
            setTimeout(function(){
                window.scrollTo(0, document.body.scrollHeight);
                console.log(document.querySelector('.input.smaller'))
                document.querySelector('.input.smaller').focus();
            },50);
        }

        context.events.appendView = function (viewName) {
            outputService.show(viewName);
        };

        context.check.isView = function (viewName) {
            return viewName && viewName.indexOf('html') > -1;
        };

        $scope.$watch(function(){
            return outputService.getLines();
        }, function (current) {
            context.data.lines = current;
            scrollToInput();
        },true);
    }
]);
