/**
 * Created by Mosh Mage on 6/2/2016.
 */
angular.module('mmConsole').constant('ViewsConstant',{
    help: { view: 'views/pages/help.html' },
    about: { view: 'views/pages/about.html' },
    work: { view: 'views/pages/work.html' },
    contact: { view: 'views/pages/contact.html' },
    unknown: { view: 'views/fillers/unknown.html' },
    newline: { view: 'views/fillers/newline.html' }
});
