angular.module('mmConsole').controller('inputController',[
    'AllowedCommands', 'OutputService',
    function inputController(allowedCommands, outputService) {
        var context = this;
        var commands = {};


        commands = {
            '/clear': outputService.resetLines,
            '/rpg': ''
        };


        context.data = {};
        context.events = {};

        context.events.makePEBKAC = function makePEBKAC() {
            var command;
            if (!context.data.pebkac) {
                context.data.pebkac = 'newline';
            }
            
            if (context.data.pebkac.indexOf('/') === 0) {
                command = Object.keys(commands).indexOf(context.data.pebkac);
                if (command > -1) {
                    commands[Object.keys(commands)[command]]();
                } else {
                    outputService.show('unknown');
                }
            } else {
                outputService.show(context.data.pebkac);
            }
            context.data.pebkac = '';
        }
    }
]);