/**
 * Created by Mosh Mage on 6/5/2016.
 */
angular.module('SocketIO',[])
    .constant('SocketConfig',{
        address: 'http://localhost:80'
    })
    .factory('SocketIO', ['$rootScope', 'SocketConfig', function ($rootScope, socketConfig) {
    var socket;

    function assignSocket() {
        socket = io(socketConfig.address);
    }

    function rootScopeApply(socket, args, callback) {
        $rootScope.$apply(function () {
            callback.apply(socket, args);
        });
    }

    return {
        raw: socket,
        hook: assignSocket,
        on: function on(eventName, callback) {
            socket.on(eventName, function () {
                rootScopeApply(socket, arguments, callback);
            });
        },
        emit: function emit(eventName, data, callback) {
            socket.emit(eventName, data, function () {
                rootScopeApply(socket, arguments, callback);
            });
        }
    }
}]);

