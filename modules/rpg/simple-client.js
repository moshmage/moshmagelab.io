/**
 * Created by Mosh Mage on 6/5/2016.
 *
 * todo: refactor this ugly-ass file.
 * damn, this file is ugly.
 *
 */
angular.module('mmConsole').controller('RRGController',['SocketIO', function(socket) {
    var context = this;
    var _player;

    context.model = {
        username: '',
        loginKey:'',
    };

    context.flags = {
        isConnected: false,
        isFlooding: false
    };

    context.data = {
        playerLocation: {},
        retrieved: [],
        username: "",
        loginKey: '',
        player: {},
        chosenMove: {
            name: ''
        }
    };

    function addLog(data) {
        context.data.retrieved.unshift(data);
        console.log(data);
    }

    function isUsernameValid() {
        return /^[a-zA-Z0-9_\-]+$/g.test(context.data.username) && context.data.username.length <= 5;
    }

    function cmdWhere() {
        socket.emit('map-self-location');
        addLog('Checking surroundings..');
    }

    function cmdBootmeup() {
        if (context.model.username) {
            context.flags.isHooking = false;
            socket.emit('bootmeup');
            addLog('Requesting login...');
        }
    }

    function cmdCloseConn() {
        socket.emit('dm-end');
        addLog('Connection closed :)');
        context.flags.isConnected = false;
    }

    function connectButtonClicked() {

        if (context.flags.isConnected) {
            cmdCloseConn();
        } else {
            Hook();
            setTimeout(cmdBootmeup,3000);
        }
    }


    function cmdHelp() {
        console.log('Available Commands',Object.keys(context.commands));
    }

    function cmdMovePlayer(direction) {
        if (direction) {
            socket.emit('map-move',{direction:direction});
            addLog('Moving ' + direction);
        }
    }

    function cmdDoBattle(withPlayer) {
        socket.emit('battle-attack', {target: withPlayer});
        addLog('Attacking ' + withPlayer);
    }

    context.commands = {
        "/where": cmdWhere,
        "/connect": cmdBootmeup,
        "/close": cmdCloseConn,
        "/help": cmdHelp
    };


    function Hook() {
        socket.hook();
        addLog("Hooking events..");
        context.flags.isHooking = true;

        socket.on('request-details', function () {
            addLog("Server requested details");
            socket.emit('player-details', {name: context.model.username, loginkey: context.model.loginKey.toString()});
        });

        socket.on('player-hello', function (data) {
            addLog("Hello from Server!");

            context.data.player = Object.create(data.player);
            // context.data.playerLocation.inMap = data.where;
            context.flags.isConnected = true;
            console.log(context, data);
        });

        socket.on('self-location', function (locationObject) {
            context.data.playerLocation = locationObject;
            console.log('got that location', locationObject);
            addLog("You checked your surroundings");
        });

        socket.on('self-moved', function () {
            addLog('You arrived');
        });

        socket.on('battle-result',function(data){
            console.log('battle-result', data);
            addLog('Battle results are in your console :x');
        });

        socket.on('dm-attacked',function(data){
            console.log('battle-result', data);
            context.data.player.levelInfo.battleStats.health = data.health;
            addLog('You were attacked by the bastard ' + data.attacker);
        });

        socket.on('dm-error', function (data) {
            addLog("Error:" + JSON.stringify(data));
        });

        socket.on('dm-flood', function (data) {
            if (data.flag) {
                addLog("Too much information going into your head..");
            }
            if (context.flags.isFlooding && !data.flag) {
                addLog("After a while, you calm down.");
            }
            context.flags.isFlooding = data.flag;
        });

        socket.on('dm-death', function(data) {
            addLog(JSON.stringify(data));
        });

        socket.on('map-inside', function(data) {
        addLog(JSON.stringify(data));
      });

        socket.on('fail', function(data) {
            addLog(data.msg);
        })

        socket.on('disconnect',function() {
            if (context.flags.isConnected) {
                context.flags.isConnected = false;
                addLog('connection lost.');
            }
        });

    }

    context.events = {
        movePlayer: cmdMovePlayer,
        attack: cmdDoBattle,
        connectButtonClicked: connectButtonClicked
    };

    context.check = {
        isUsernameValid: isUsernameValid
    }

}]);
