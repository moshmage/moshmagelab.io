/**
 * Created by Mosh Mage on 6/4/2016.
 */
angular.module('mmConsole').constant('WorkConstant', [
      {
    companyName: 'BEPRO Network',
    jobTitle: 'CTO / Head of Development',
    date: 'Ago 2021 — Present',
    projectScope: [
      'create and curate bepro-js SDK, a web3 toolkit used for templating and wrappering contracts to be interacted throught Javascript',
    ],
    performedFunctions: [
      'Lead the teams and community around the BEPRO Network framework',
      'Plan and design High level changes to the BEPRO ecosystem',
      'Review and curate the proposals on the BEPRO Network'
    ],
    technicalEnv: ['Javascript', 'ReactJS', 'Typescript', 'Solidity'],
    liveExample: 'https://development.bepro.network/'
  },
    {
    companyName: 'Vigil365 / Good2Rent',
    jobTitle: 'Frontend Lead',
    date: 'Jan 2020 — Jul 2021',
    projectScope: [
      'Create WebUI for a online Rental Passport which enables tenants to start their rental process and agents to catch those tenants',
      'Integrate login and register with YOTI, etc..',
    ],
    performedFunctions: [
      'Story planning',
      'Responsible for design, architecture, development, structure, and implementation of the Frontend Application',
      'Lead team members',
      'Code review, Task management',
      'Help UX team with design choices'
    ],
    technicalEnv: ['Javascript', 'Vue2', 'Typescript', 'RxJs', 'Stripe', 'YOTI'],
    liveExample: 'https://good2rent.co.uk/'
  },
  {
    companyName: 'ReadinessIT / Entel',
    jobTitle: 'Senior Javascript / Squad Lead',
    date: 'Feb 2017 — Dec 2019',
    projectScope: [
      'Order Capture for Mobile Prepaid (and Postpaid) Entel Chile/Peru',
      'Create a Order Care and Point of Sale web application in Angular (OneClick)',
    ],
        performedFunctions: [
      'Responsible for design, architecture, development, structure, and implementation of the Frontend Application',
      'Lead team members into Angular world',
      'Code review, Task management, DevOps (building and deploying)',
      'Improved company wide the automation situation (mainly on FE)',
      'Discuss and align with UX for a better integration'
    ],
    technicalEnv: ['Javascript', 'Bootstrap', 'Angular', 'NodeJs', 'Typescript'],
    liveExample: false
  },
  {
    companyName: 'NOSSA / Carlsberg',
    jobTitle: 'Javascript Developer',
    date: 'April 2016 — April 2016',
    projectScope: [
      'Implement http://www.carlsbergeuroexperience.pt/ website – A promotional website with challenges and ranking system.',
      'Create & Structure a mini-framework thought for the existing and new promotional challenges website'
    ],
    performedFunctions: [
      'Programming new functionality and features related to UI/UX on www.carlsbergeuroexperience.pt',
      "Created a new framework, and its' documentation, that will be used in future promotional websites and act as their main back-office throughout their websites.",
      'Advised the company, and coworkers, on new methodologies and technologies so delivery and integration is easily achieved.'
    ],
    technicalEnv: ['Javascript', 'jQuery', 'CSS3', 'HTML5', 'PHP', 'Smarty'],
    liveExample: false
  },
  {
    companyName: 'BLiP / Betfair',
    jobTitle: 'Fullstack Javascript Developer',
    date: 'October 2014 — May 2016',
    projectScope: [
      'de-brand International/European Betfair website',
      'Implement Betfair website in New Jersey'
    ],
    performedFunctions: [
      'Advanced AngularJS development',
      'Full E2E and Unit testing',
      'Grunt tasks and micro-services',
      'SCRUM'
    ],
    technicalEnv: ['Javascript', 'AngularJS', 'NodeJs', 'CSS3', 'HTML5', 'Sass'],
    liveExample: 'http://us.betfair.com'
  },
  {
    companyName: 'Bridgestone',
    jobTitle: 'Frontend & Backend Developer',
    date: 'February 2014 — October 2014',
    projectScope: [
      'Intranet SOAP/REST Application with Navision Database',
    ],
    performedFunctions: [
      'Revamped their Javascript to module-pattern',
      'Ajaxified the process',
      'Small PHP API to ease the Ajaxification',
      'Translator console task'
    ],
    technicalEnv: ['Javascript', 'jQuery', 'Noty', 'Data Tables', 'Bootstrap', 'PHP', 'Codeigniter'],
    liveExample: false
  },
  {
    companyName: 'Toyota @ Belgium',
    jobTitle: 'Frontend Developer',
    date: 'September 2012 — September 2013',
    projectScope: [
      '2014~Current website',
    ],
    performedFunctions: [
      'Development of various modules and components',
      'Object Oriented Programming',
    ],
    technicalEnv: ['Javascript', 'jQuery', 'Bootstrap', 'NPM', 'Bower', 'Less', 'HTML5', 'CSS3'],
    liveExample: 'http://toyota.nl'
  }
]);

angular.module('mmConsole').controller('WorkController', [
  'WorkConstant',
  function (workConstant) {
    var context = this;
    context.data = {};
    context.data.past = workConstant;
  }
]);
